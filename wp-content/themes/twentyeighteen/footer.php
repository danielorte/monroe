<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.2
 */

?>

		</div><!-- #content -->

		<footer id="colophon" class="site-footer" role="contentinfo">
			<div class="wrap">
				<?php
get_template_part('template-parts/footer/footer', 'widgets');

if (has_nav_menu('social')): ?>
					<nav class="social-navigation" role="navigation" aria-label="<?php esc_attr_e('Footer Social Links Menu', 'twentyseventeen');?>">
						<?php
wp_nav_menu(array(
    'theme_location' => 'social',
    'menu_class' => 'social-links-menu',
    'depth' => 1,
    'link_before' => '<span class="screen-reader-text">',
    'link_after' => '</span>' . twentyseventeen_get_svg(array('icon' => 'chain')),
));
?>
					</nav><!-- .social-navigation -->
				<?php endif;

get_template_part('template-parts/footer/site', 'info');
?>
			</div><!-- .wrap -->
		</footer><!-- #colophon -->
	</div><!-- .site-content-contain -->
</div><!-- #page -->
<?php wp_footer();?>

    <footer class="main-footer footer red lighten-3 white-text">
        <div class="columns is-multiline">
            <div class="column is-one-quarter-desktop is-full-touch">
                <h1 class="title is-4 white-text">¿Quienes somos?</h1>
                <p>
                    Somos una empresa joven que buscamos que la moda sea accesible sin sacrificar el estilo. Moda a temporal y versátil. Diseñamos,
                    confeccionamos y comercializamos nuestras creaciones.
                </p>
                <br>
                <p>
                    <a href="mailto:viste@missmonroemoda.com" class="white-text">
                        <i class="material-icons">email</i>
                        <span>viste@missmonroemoda.com</span>
                    </a>
                </p>
                <p>
                    <a href="tel:3781052289" class="white-text">
                        <i class="material-icons">local_phone</i>
                        <span>378-1052-289</span>
                    </a>
                </p>
                <p>
                    <a href="tel:3151128088" class="white-text">
                        <i class="material-icons">local_phone</i>
                        <span>315-1128-088</span>
                    </a>
                </p>
            </div>
            <div class="column is-half-desktop is-full-touch">
                <h1 class="title is-4 white-text">Ultimas noticias</h1>
                <template v-for="post in posts">
                    <div class="box" style="padding: 1rem;">
                        <article class="media">
                            <div class="media-left is-hidden-mobile">
                                <figure class="image is-128x128">
                                    <img :src="post._embedded['wp:featuredmedia'][0].source_url" alt="Image">
                                </figure>
                            </div>
                            <div class="media-content">
                                <div class="content">
                                    <p class="title is-6">
                                        <b>{{post.title.rendered}}</b>
                                        <small>@{{post._embedded.author[0].slug}} {{postTime(post.date)}}</small>
                                    </p>
                                    <p>
                                        <small v-html="post.excerpt.rendered"></small>
                                    </p>
                                </div>
                            </div>
                        </article>
                    </div>
                </template>
            </div>
            <div class="column">
                <h1 class="title is-4 white-text">Enlaces</h1>
                <ul>
                    <li>
                        <a class="white-text">
                            Sobre Nosotros
                        </a>
                    </li>
                    <li>
                        <a class="white-text">
                            Preguntas Frecuentes
                        </a>
                    </li>
                    <li>
                        <a class="white-text">
                            Política de Compra
                        </a>
                    </li>
                    <li>
                        <a class="white-text">
                            Política de Privacidad
                        </a>
                    </li>
                    <li>
                        <a class="white-text">
                            Terminos de uso
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="footer-copyright white-text">Moda Miss Monroe &copy;2018</div>

    </footer>
</body>
</html>
