<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes();?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo('charset');?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head();?>
</head>

<body <?php body_class();?>>
<div id="page" class="site">
	<section class="main-header hero is-small">
        <!-- Hero head: will stick at the top -->
        <div class="hero-head">
            <nav class="navbar is-fixed-top">
                <div class="container">
                    <div class="navbar-brand">
                        <a class="navbar-item brand red-text text-lighten-3" href="/">
                            Miss Monroe
                        </a>
                        <span class="navbar-burger burger" data-target="navbarMenuHeroA">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </div>
                    <div id="navbarMenuHeroA" class="navbar-menu">
                        <div class="navbar-end">
                            <a class="navbar-item" href="/">
                                Novedades
                            </a>
                            <a class="navbar-item" href="/blog">
                                Blog
                            </a>
                            <router-link :to="{ name: 'custom-made' }" class="navbar-item" v-if="user_type != 1">Has tu pedido</router-link>
                            <router-link :to="{ name: 'contact' }" class="navbar-item" v-if="user_type != 1">Contáctanos</router-link>
                            <span class="navbar-item" v-if="!isLoggedIn">
                                <router-link :to="{ name: 'login' }" class="button is-info">
                                    Entrar
                                </router-link>
                            </span>
                            <span class="navbar-item" v-if="!isLoggedIn">
                                <router-link :to="{ name: 'register' }" class="button is-info">
                                    Registrarme
                                </router-link>
                            </span>
                            <!-- <router-link :to="{ name: 'userboard' }" class="navbar-item" v-if="isLoggedIn && user_type == 0"> Hola, {{name}}</router-link>
                            <router-link :to="{ name: 'admin' }" class="navbar-item" v-if="isLoggedIn && user_type == 1"> Hola, {{name}}</router-link>
                            <a class="navbar-item" v-if="isLoggedIn" @click="logout">Salir</a> -->
                        </div>
                    </div>
                </div>
            </nav>
        </div>

        <!-- Hero content: will be in the middle -->
        <div class="hero-body" v-if="user_type != 1">
            <div class="container has-text-centered brand">
                <router-link :to="{ name: 'home' }">
                    <h1 class="title grey-text text-darken-3">
                        Miss Monroe
                    </h1>
                    <h2 class="subtitle grey-text text-darken-3">
						Viste a tu medida
                        <img src="<?php echo get_template_directory_uri();?>/assets/img/dividers/divider-1.png" class="img-divider">
                    </h2>
                </router-link>
                <div class="links">
                    <a href="mailto:viste@missmonroemoda.com" class="grey-text">
                        <i class="material-icons red-text text-lighten-3">email</i>
                        <span>viste@missmonroemoda.com</span>
                    </a>
                    <a href="tel:3781052289" class="grey-text">
                        <i class="material-icons red-text text-lighten-3">local_phone</i>
                        <span>378-1052-289</span>
                    </a>
                    <a href="tel:3151128088" class="grey-text">
                        <i class="material-icons red-text text-lighten-3">local_phone</i>
                        <span>315-1128-088</span>
                    </a>
                </div>
            </div>
        </div>

        <!-- Hero footer: will stick at the bottom -->
        <div class="hero-foot" :class="{'is-logged-admin':user_type == 1}">
            <nav class="red lighten-3 categorias">
                <div class="container">
                    <a href="https://www.facebook.com/MissMonroeModa" target="_blank" class="navbar-brand-icon" title="Facebook">
                        <img src="<?php echo get_template_directory_uri();?>/assets/img/brands/fb/icons8-facebook-26.png">
                    </a>
                    <a href="https://www.instagram.com/miss_monroe_moda" target="_blank" class="navbar-brand-icon" title="Instagram">
                        <img src="<?php echo get_template_directory_uri();?>/assets/img/brands/instagram/icons8-instagram-26.png">
                    </a>
                    <div class="dropdown is-hoverable" v-for="categoria in categorias">
                        <div class="dropdown-trigger">
                            <a class="white-text" aria-haspopup="true" aria-controls="dropdown-menu4">
                                <span>{{categoria.nombre}}</span>
                            </a>
                        </div>
                        <div class="dropdown-menu" id="dropdown-menu4" role="menu" v-if="categoria.categorias.length">
                            <div class="dropdown-content">
                                <div class="dropdown-item">
                                    <div class="menu">
                                        <template v-for="subcategoria in categoria.categorias">
                                            <a class="menu-label red-text text-lighten-3">
                                                {{subcategoria.nombre}}
                                            </a>
                                            <ul class="menu-list">
                                                <li v-for="data in subcategoria.subcategorias">
                                                    <a>{{data.nombre}}</a>
                                                </li>
                                            </ul>
                                        </template>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </section>
	<a class="skip-link screen-reader-text" href="#content"><?php _e('Skip to content', 'twentyseventeen');?></a>

	<header id="masthead" class="site-header" role="banner">

		<?php get_template_part('template-parts/header/header', 'image');?>

		<?php if (has_nav_menu('top')): ?>
			<div class="navigation-top">
				<div class="wrap">
					<?php get_template_part('template-parts/navigation/navigation', 'top');?>
				</div><!-- .wrap -->
			</div><!-- .navigation-top -->
		<?php endif;?>

	</header><!-- #masthead -->

	<?php

/*
 * If a regular post or page, and not the front page, show the featured image.
 * Using get_queried_object_id() here since the $post global may not be set before a call to the_post().
 */
if ((is_single() || (is_page() && !twentyseventeen_is_frontpage())) && has_post_thumbnail(get_queried_object_id())):
    echo '<div class="single-featured-image-header">';
    echo get_the_post_thumbnail(get_queried_object_id(), 'twentyseventeen-featured-image');
    echo '</div><!-- .single-featured-image-header -->';
endif;
?>

	<div class="site-content-contain">
		<div id="content" class="site-content">
